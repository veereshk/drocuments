﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core;

namespace Service
{
  public  interface IService
    {
    }

  public interface IUserService
  {

        int CreateUser(tbl_UserInfo objuserinfo);
        int CreateFamilyMember(tbl_UserInfo objuserinfo);
        int ValidateAccount(string authenticationToken);
        tbl_UserInfo ValidateUser(tbl_LogIn objlogin);
        int ResetPassword(string token, string newpassword);
        int SetMemberAsUser(int userid, int familymemberid, string familymemberemailid);
        int AddTokenForForgotPassword(string EmailAddress, string AuthToken);
    


  }
  public interface IPrescriptionService
  {
  }
  public interface IDocumentService
  { }


}
