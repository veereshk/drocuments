﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.Web.WebPages.OAuth;
using System.Configuration;
using WebMatrix.WebData;


namespace Drocuments
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public class oAuthConfig
        {

            public static void RegisterProviders()
            {
                // OAuthWebSecurity.RegisterGoogleClient();

                OAuthWebSecurity.RegisterClient(new Drocuments.GoogleCustomClient(), "Google", null);

                OAuthWebSecurity.RegisterFacebookClient(appId: "1610863505809828", appSecret: "31d96542de2872c201d714351aa8510d");
            }

        }
        protected void Application_Start()
        {

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            oAuthConfig.RegisterProviders();

            if (!WebSecurity.Initialized)
            {
            WebSecurity.InitializeDatabaseConnection("DrocumentsWebSequirityEntities", "tbl_UserInfo", "UserId", "Email", autoCreateTables: true);
            }
        }
    }
}