﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using System.IO;
using System.Net;
using System.Web.Configuration;
namespace Drocuments
{
    public class MailMessage
    {
        private string _mailServer;

        #region mail class accessors

        public string Cc { get; set; }

        public string Bcc { get; set; }

        public string Body { get; set; }

        public bool IsBodyHtml { get; set; }


        public string Subject { get; set; }

        public string MailServer
        {
            get { return _mailServer ?? (_mailServer = "smtp.gmail.com"); }
            set { _mailServer = value; }
        }

        public string From { get; set; }

        public string To { get; set; }

        public string MailFormat { get; set; }

        public string Attachments { get; set; }

        public Exception MailException { get; set; }

        #endregion


        public static string SendSms(string Url, string User, string password, decimal Mobile_Number, string Message, string MType)
        {
            try
            {
                string stringpost = "" + Url + "?User=" + User + "&passwd=" + password + "&mobilenumber=" + Mobile_Number.ToString() + "&message=" + Message + "&MTYPE=" + MType;
                WebRequest request = HttpWebRequest.Create(stringpost);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream s = (Stream)response.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                string dataString = readStream.ReadToEnd();
                response.Close();
                s.Close();
                readStream.Close();

                return "";
            }
            catch
            {
                return "";
            }
        }


        public bool SendMail(string toaddress, string body, string subject)
        {
            From = "drocuments@gmail.com";
            //From = "sudheerg38@gmail.com";

            //To = "info@healthurwealth.com";
            var msg = new System.Net.Mail.MailMessage();

            if (Body != null) msg.Body = Body;

            if (Cc != null) msg.CC.Add(Cc);
            if (toaddress != null) msg.To.Add(toaddress);
            if (From != null) msg.From = (new MailAddress(From));

            //  if (Bcc != null) msg.Bcc.Add(Bcc);
            if (subject != null) msg.Subject = subject;
            msg.Body = body;
            msg.IsBodyHtml = true;
            //set attachments here

            //if (Attachments != null)
            //{
            //    var delim = new[] { ',' };

            //    foreach (var data in Attachments.Split(delim).Select(sattach => new Attachment(GetNewFileNamePath() + @"\" + sattach)))
            //    {
            //        // Add the file attachment to this e-mail message.
            //        msg.Attachments.Add(data);
            //    }
            //}
            //var smtp = new System.Net.Mail.SmtpClient();
            var client = new SmtpClient(MailServer)
            {
                UseDefaultCredentials = true,

                Credentials = new System.Net.NetworkCredential("drocuments@gmail.com", "dra@12345"),

                //Credentials = new System.Net.NetworkCredential("dlmssv@gmail.com", "dlmssvsanthu"),
                //EnableSsl = true
            };
            //client.Host = "smtpout.secureserver.net";
            //client.Port = 3535;
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.EnableSsl = true;
            // from web.config files
            //client.Credentials = smtp.Credentials;

            client.Send(msg);



            //var msg = new System.Net.Mail.MailMessage();
            //System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

            //msg.From = new MailAddress("info@healthurwealth.com");
            //msg.To.Add("nagulsk1991@gmail.com");
            //msg.Subject = "test";
            //msg.Body = "Test Content";
            ////msg.Priority = MailPriority.High;


            //using (SmtpClient client = new SmtpClient())
            //{
            //    //client.EnableSsl = true;
            //    client.UseDefaultCredentials = false;
            //    client.Credentials = new NetworkCredential("info@healthurwealth.com", "JYbsaE3R#$");
            //    client.Host = "mail.healthurwealth.com";
            //    //client.Port = 587;
            //    //client.DeliveryMethod = SmtpDeliveryMethod.Network;

            //    client.Send(msg);
            //}

            return true;
        }

        private string GetNewFileNamePath()
        {
#pragma warning disable 612,618
            {
                var filePath = ConfigurationSettings.AppSettings["MailFilePath"];
                // ReSharper disable NotResolvedInText
                if (filePath == null) throw new ArgumentNullException("filePath");
                // ReSharper restore NotResolvedInText
                return filePath;
            }
        }

        public bool DeleteSentFile()
        {
            if (Attachments != null)
            {
                var delim = new[] { ',' };
                foreach (var fi in from sattach in Attachments.Split(delim) where File.Exists(GetNewFileNamePath() + @"\" + sattach) select new FileInfo(GetNewFileNamePath() + @"\" + sattach))
                {
                    fi.Delete();
                }
            }
            return true;
        }

    }
}