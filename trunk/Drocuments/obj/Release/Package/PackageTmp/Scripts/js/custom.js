﻿menuSelect();
function menuSelect() {
    var url = window.location.pathname;

    $(".dashboard-menu a").each(function () {
        //Chrome Support for .contains() START
        if (!('contains' in String.prototype)) {
            String.prototype.contains = function (str, startIndex) {
                return -1 !== String.prototype.indexOf.call(this, str, startIndex);
            };
        }
        //Chrome Support for .contains() END
        if (url == $(this).attr('href')) {
            $(this).addClass("active");
        }
    });

}
function GetprofileInfo() {

    var json = CheckLogin();
    
        jQuery.support.cors = true;
        $.ajax({
            url: '../api/Master/GetProfileDetails?id=' + json['UserId'],
            type: 'GET',
            dataType: 'json',
            success: function (data) {

                $("#FirstName").val(data.Result.FirstName),
    $("#LastName").val(data.Result.LastName),
    $("#Address").val(data.Result.Address),
    $("#ContactNumber").val(data.Result.MobileNo)
               
            }

        });
    
}
var tblmedicine = new Array();
var tbldrocument = new Array();
var drocument = '';
//global variables 
 


function ClearGlobalCacheData()
{
    tblmedicine.length = 0;
    tbldrocument.length = 0;
    drocument = '';
    documentextenstion = '';
    base64string = '';


}
$(document).ready(function () {

    $('#fileUpload').change(function (evt) {
        var filename = $(this).val();
        var documentextenstion = filename.replace(/^.*\./, '');
        var docname = $(this).parents('.drocument').find('.drocumentname').val();
            var fileReader = new FileReader(),
                files = this.files,
                file;
            if (!files.length) {
                return;
            }
            file = files[0];
            fileReader.readAsDataURL(file);
            fileReader.onload = function (_file) {
                var url = _file.target.result;
                tbldrocument.push({ drocName: docname, fileUpload: url.substring(url.indexOf("base64") + 7), Format: documentextenstion });
            };
        });
       
    $("#addmedicine").click(function () {
        $('.medicine').each(function () {
            var medic = getmedicine($(this));
            tblmedicine.push(medic);
        });
        $('button[class=close]').click();
    });
    $("#adddrocument").click(function () {
        $('button[class=close]').click();
    });

    $("#savedrocument").click(function () {
        debugger;
        drocumentdata = {
            DocumentName: $("#DocumentName").val(),
            DateOfVisit: $("#DateOfVisit").val(),
            NextVisit: $("#NextVisit").val(),
            HealthproblemDescription: $("#HealthproblemDescription").val(),
            medicines: tblmedicine, dro: tbldrocument
        };




        var DTO = JSON.stringify(drocumentdata);
        $.ajax({
            url: '/Drocuments/Add', //calling Web API controller product
            cache: false,
            type: 'POST',async:false,
            contentType: 'application/json; charset=utf-8',
            data: DTO,
            dataType: 'json',
            //contentType: false,
            //processData: false,
            success: function (data) {
                alert('added');
                ClearGlobalCacheData();
            }
        }).fail(
        function (xhr, textStatus, err) {
            alert(err);
        });
    });
});

function getmedicine(ele) {
    var Name = ele.find("#Nameofmedicine").val();
    var Durat = ele.find("#Duration").val();
    var qty = ele.find("#Quantity").val();
    var names = ele.find('.dosageClass:checked').map(function () {
        return this.value;
    }).get().join(', ')

    return { Nameofmedicine: Name, Duration: Durat, Dosage: names,Quantity:qty };
}






$(document).ready(function () {

    $('#fileUpload').change(function (evt) {
        var filename = $(this).val();
        var documentextenstion = filename.replace(/^.*\./, '');
        var docname = $(this).parents('.drocument').find('.drocumentname').val();
        var fileReader = new FileReader(),
            files = this.files,
            file;
        if (!files.length) {
            return;
        }
        file = files[0];
        fileReader.readAsDataURL(file);
        fileReader.onload = function (_file) {
            var url = _file.target.result;
            tbldrocument.push({ drocName: docname, fileUpload: url.substring(url.indexOf("base64") + 7), Format: documentextenstion });
        };
    });

   



    $$(function () {
        $('.edit-mode').hide();
        $('.edit-droc, .cancel-droc').on('click', function () {
            var tr = $(this).parents('tr:first');
            tr.find('.edit-mode, .display-mode').toggle();
        });

        $('.save-droc').on('click', function () {
            debugger;
            var tr = $(this).parents('tr:first');
            var fileUpload = tr.find("#fileUpload").val();
            var drocName = tr.find("#drocName").val();
            var DocumentID = tr.find(".prescriptionmedicineid").val();
            //tr.find("#lblName").text(HealthproblemDescription);
            //tr.find("#lblSurName").text(DateOfVisit);
            //tr.find("#PrescriptionId").text(PrescriptionId)

            tr.find('.edit-mode, .display-mode').toggle();
            var UserModel =
            {
                "fileUpload": fileUpload,
                "drocName": drocName,
                "DocumentID": DocumentID
            };
            $.ajax({
                url: '/Drocuments/EditDrocuments/',
                data: JSON.stringify(UserModel),
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    alert(data);
                }
            });

        });
    })
});
