﻿using Amazon;
using Drocuments.Models.DTOs;
using Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;


namespace Drocuments.Controllers
{
    public class DoctorsController : Controller
    {
        //
        // GET: /Doctors/
        DrocumentsEntities dc = new DrocumentsEntities();

        public ActionResult Index()
        {
            if (TempData["Message"] != null)
            {
                ViewBag.Message = TempData["Message"].ToString();
            }
            int id = WebSecurity.CurrentUserId;
            // dc.Configuration.ProxyCreationEnabled = false;

            List<doctor> data = dc.doctors.Where(p => p.id == id).ToList();
            return View(data);
          
        }
        public ActionResult Add()
        {
            Doctorsmodel model = new Doctorsmodel();
            int id = WebSecurity.CurrentUserId;
            model.id = id;
            return View(model);

        }
        [HttpPost ]
        public ActionResult Add(Doctorsmodel objdoc)
        {
            doctor doc = new doctor
            {
                id = objdoc.id,
                name = objdoc.name,
                specialization_id = objdoc.specialization_id,
                clinic_name = objdoc.clinic_name,
                address = objdoc.address,
                qualification = objdoc.qualification,
                rating = objdoc.rating
            };
            dc.doctors.Add(doc);
            dc.SaveChanges();
            Doctorsmodel model = new Doctorsmodel();


            return View(model);

        }

     
    }
}
