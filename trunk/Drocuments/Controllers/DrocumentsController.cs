﻿using Amazon;
using Drocuments.Models.DTOs;
using Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace Drocuments.Controllers
{
    public class DrocumentsController : Controller
    {

        DrocumentsEntities dc = new DrocumentsEntities();
        //
        // GET: /Drocuments/
        public ActionResult Index()
        {
            if (TempData["Message"] != null)
            {
                ViewBag.Message = TempData["Message"].ToString();
            }
            int id = WebSecurity.CurrentUserId;
            // dc.Configuration.ProxyCreationEnabled = false;
            var data = dc.tbl_Prescription.Where(p => p.PatientId == id&&p.Status==true).ToList();
            return View(data);
        }



        [HttpPost]
        public JsonResult fileDelete(Int32 drocid)
        {
            //var items = from p in dc.tbl_Prescription
            //               join d in dc.tbl_Document on p.PrescriptionId equals d.PrescriptionID
            //               join c in dc.tbl_PrescribedMedicine on d.PrescriptionID equals c.PrescriptionId
            //             where p.PrescriptionId == drocid


            tbl_Document dr = dc.tbl_Document.Where(d => d.PrescriptionID == drocid).FirstOrDefault();
            tbl_PrescribedMedicine pd = dc.tbl_PrescribedMedicine.Where(p => p.PrescriptionId == drocid).FirstOrDefault();
            tbl_Prescription product = dc.tbl_Prescription.Where(x => x.PrescriptionId == drocid).FirstOrDefault();
            dr.Status = false;
            product.Status = false;
            pd.Status = false;
            dc.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteMedicine(Int32 drocid)
        {


            tbl_PrescribedMedicine pd = dc.tbl_PrescribedMedicine.Where(p => p.PrescribedMedicineId == drocid).FirstOrDefault();
            pd.Status = false;
            dc.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public JsonResult DeleteDrocument(Int32 id)
        {


            tbl_Document pd = dc.tbl_Document.Where(p => p.DocumentID == id).FirstOrDefault();
            pd.Status = false;
            dc.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
            //Drocumentdetails dds = new Drocumentdetails();

            //var items = from p in dc.tbl_Prescription
            //                join d in dc.tbl_Document on p.PrescriptionId equals d.PrescriptionID
            //                join c in dc.tbl_PrescribedMedicine on d.PrescriptionID equals c.PrescriptionId
            //                where p.PatientId == id
            //                select new
            //                {
            //                    p.PrescriptionId,
            //                    p.HealthproblemDescription,
            //                    p.NextVisit,
            //                    p.DateOfVisit,
            //                    d.DocumentPath,
            //                    d.DocumentName,
            //                    c.NameOfMedicine,
            //                    c.Duration,
            //                    c.Dosage
            //                };

            //foreach (var ds in items)
            //{
            //    //dds.DocumentID = ds.DocumentID;
            //      dds.HealthproblemDescription=ds.HealthproblemDescription;
            //                    dds.NextVisit=ds.NextVisit;
            //                    dds.DateOfVisit=ds.DateOfVisit;
            //                    dds.DocumentPath=ds.DocumentPath;
            //                    dds.DocumentName=ds.DocumentName;
            //                    dds.NameOfMedicine=ds.NameOfMedicine;
            //                    dds.Duration=ds.Duration;
            //                    dds.Dosage = ds.Dosage;
            //}
            // int id = WebSecurity.CurrentUserId;

            //IEnumerable<tbl_Prescription> model = null;

            //model = (from p in dc.tbl_Prescription where p.PatientId == WebSecurity.CurrentUserId select p).ToList();
            ////join d in dc.tbl_Document on p.PrescriptionId equals d.PrescriptionID
            ////join pm in dc.tbl_PrescribedMedicine on p.PrescriptionId equals pm.PrescriptionId
            ////where p.PatientId==id
            ////select new
            ////{

            //dc.Configuration.ProxyCreationEnabled = false;
            if (TempData["Message"] != null)
            {
                ViewBag.Message = TempData["Message"].ToString();
            }
            ////var dataa = dc.tbl_Document.Where(d => d.DocumentID == drocid);

            //var data = dc.tbl_Prescription.Where(p => p.PatientId == WebSecurity.CurrentUserId);
            //return View(model);
        }




        [HttpPost]
        public JsonResult Add(AddaDrocumentDTO objadddrocdto)
        {

            int faileddocuments = 0;
            string myBucketName = ConfigurationManager.AppSettings["bucketname"]; //your s3 bucket name goes here
            string s3DirectoryName = "";
            string s3FileName = string.Format(@"{0}.txt", Guid.NewGuid());
            string AccessKey = ConfigurationManager.AppSettings["AWSAccessKey"].ToString();
            string SecretKey = ConfigurationManager.AppSettings["AWSSecretKey"].ToString();

            //AmazonUploader.sendMyFileToS3(fileUpload = prop.fileUpload, myBucketName, s3DirectoryName, s3FileName, ConfigurationManager.AppSettings["AWSAccessKey"], ConfigurationManager.AppSettings["AWSSecretKey"], Amazon.RegionEndpoint.USEast1);


            int id = WebSecurity.CurrentUserId;

            var droc = new tbl_Prescription
            {
                PatientId = id,
                DateOfVisit = objadddrocdto.DateOfVisit,
                NextVisit =objadddrocdto.NextVisit,
                HealthproblemDescription = objadddrocdto.HealthproblemDescription,
                FileName = objadddrocdto.FileName,
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now,
                Status = true,
                DescriptionDate = DateTime.Now,
                CreatedBy = id,
                UpdatedBy = id
            };

            dc.tbl_Prescription.Add(droc);
            dc.SaveChanges();

            if (objadddrocdto.dro != null)
            {
                foreach (Files prop in objadddrocdto.dro)
                {
                    if (AmazonUploader.sendMyFileToS3(prop.fileUpload, prop.Format, myBucketName, "", s3FileName, AccessKey, SecretKey, RegionEndpoint.USWest2))
                    {
                        var drocument = new tbl_Document
                        {
                            UpdatedDate = System.DateTime.Now,
                            CreatedDate = System.DateTime.Now,
                            DocumentName = prop.drocName,
                            CanShare = true,
                            Status = true,
                            DocumentPath = ConfigurationManager.AppSettings["awsUrl"].ToString() + ConfigurationManager.AppSettings["bucketname"].ToString() + "/" + s3FileName,
                            UpdatedBy = "",
                            CreatedBy = id
                        };
                        drocument.PrescriptionID = droc.PrescriptionId;
                        dc.tbl_Document.Add(drocument);
                        dc.SaveChanges();
                    }
                    else
                    {
                        faileddocuments++;
                    }

                };
            }

            // Push file to aws

            if (objadddrocdto.medicines != null)
            {
                foreach (MedicinesDTO1 file in objadddrocdto.medicines)
                {

                    var pris = new tbl_PrescribedMedicine
                    {
                        Duration = file.Duration,
                        NameOfMedicine = file.Nameofmedicine,
                        // PrescriptionId = 7,
                        Quantity=file.Quantity,
                        Dosage = file.Dosage,
                        Status=true,
                        CreatedDate = System.DateTime.Now,
                        UpdatedDate = System.DateTime.Now
                    };
                    pris.PrescriptionId = droc.PrescriptionId;
                    dc.tbl_PrescribedMedicine.Add(pris);
                    dc.SaveChanges();
                }
            }
            return Json("Drocument Added Successfully", JsonRequestBehavior.AllowGet);
        }




        [HttpGet]
        public ActionResult Edit(Int32 drocid)
        {
            AddaDrocumentDTO droc = new AddaDrocumentDTO();
            int id = WebSecurity.CurrentUserId;
            dc.Configuration.ProxyCreationEnabled = false;
            //var data = (from p in dc.tbl_Prescription where p.PrescriptionId == drocid select p).ToList();
            tbl_Prescription data = dc.tbl_Prescription.Single(p => p.PrescriptionId == drocid&&p.Status==true);
            droc.DateOfVisit = data.DateOfVisit;
            droc.NextVisit = data.NextVisit;
            droc.HealthproblemDescription = data.HealthproblemDescription;
            droc.FileName = data.FileName;
            droc.PrescriptionId = data.PrescriptionId;
            List<tbl_PrescribedMedicine> pris = dc.tbl_PrescribedMedicine.Where(pd => pd.PrescriptionId == drocid&&pd.Status==true).ToList();
            List<tbl_Document> drocument = dc.tbl_Document.Where(d => d.PrescriptionID == drocid&&d.Status==true).ToList();

            droc.medicines = pris.Select(x => new MedicinesDTO1() { PrescribedMedicineId = x.PrescribedMedicineId, Dosage = x.Dosage, Duration = x.Duration, Nameofmedicine = x.NameOfMedicine, Quantity = x.Quantity }).ToList();
            droc.dro = drocument.Select(x => new Files() { DocumentID = x.DocumentID, drocName = x.DocumentName, fileUpload = x.DocumentPath }).ToList();

            return View(droc);
        }
        [HttpPost]
        public ActionResult Edit(AddaDrocumentDTO editdroc)
        {


            int faileddocuments = 0;
            string myBucketName = ConfigurationManager.AppSettings["bucketname"]; //your s3 bucket name goes here
            string s3DirectoryName = "";
            string s3FileName = string.Format(@"{0}.txt", Guid.NewGuid());
            string AccessKey = ConfigurationManager.AppSettings["AWSAccessKey"].ToString();
            string SecretKey = ConfigurationManager.AppSettings["AWSSecretKey"].ToString();

            int id = WebSecurity.CurrentUserId;
            var EditDrocument = dc.tbl_Prescription.First(u => u.PrescriptionId == editdroc.PrescriptionId);



            EditDrocument.PatientId = id;
            EditDrocument.DateOfVisit = editdroc.DateOfVisit;
            EditDrocument.NextVisit = editdroc.NextVisit;
            EditDrocument.HealthproblemDescription = editdroc.HealthproblemDescription;
            EditDrocument.FileName = editdroc.FileName;
            //EditDrocument. CreatedDate = DateTime.Now;
            EditDrocument.UpdatedDate = DateTime.Now;
            EditDrocument.Status = true;
            EditDrocument.DescriptionDate = DateTime.Now;
            EditDrocument.CreatedBy = id;
            EditDrocument.UpdatedBy = id;
            dc.SaveChanges();


            if (editdroc.dro != null)
            {
                foreach (Files prop in editdroc.dro)
                {
                    if (AmazonUploader.sendMyFileToS3(prop.fileUpload, prop.Format, myBucketName, "", s3FileName, AccessKey, SecretKey, RegionEndpoint.USWest2))
                    {
                        var drocument = new tbl_Document
                        {
                            UpdatedDate = System.DateTime.Now,
                            CreatedDate = System.DateTime.Now,
                            DocumentName = prop.drocName,
                            CanShare = true,
                            Status = true,
                            DocumentPath = ConfigurationManager.AppSettings["awsUrl"].ToString() + ConfigurationManager.AppSettings["bucketname"].ToString() + "/" + s3FileName,
                            UpdatedBy = "",
                            CreatedBy = id
                        };
                        drocument.PrescriptionID = EditDrocument.PrescriptionId;
                        dc.tbl_Document.Add(drocument);
                        dc.SaveChanges();
                    }
                    else
                    {
                        faileddocuments++;
                    }

                };
            }

            // Push file to aws

            if (editdroc.medicines != null)
            {
                foreach (MedicinesDTO1 file in editdroc.medicines)
                {

                    var pris = new tbl_PrescribedMedicine
                    {
                        Duration = file.Duration,
                        NameOfMedicine = file.Nameofmedicine,
                        // PrescriptionId = 7,
                        Quantity = file.Quantity,
                        Dosage = file.Dosage,
                        CreatedDate = System.DateTime.Now,
                        UpdatedDate = System.DateTime.Now
                    };
                    pris.PrescriptionId = EditDrocument.PrescriptionId;
                    dc.tbl_PrescribedMedicine.Add(pris);
                    dc.SaveChanges();
                }
            }

            return Json("Drocument Added Successfully", JsonRequestBehavior.AllowGet);
        }







        [HttpGet]
        public ActionResult EditDrocument(Int32 drocid)
        {
            AddaDrocumentDTO droc = new AddaDrocumentDTO();
            int id = WebSecurity.CurrentUserId;
            dc.Configuration.ProxyCreationEnabled = false;
            //var data = (from p in dc.tbl_Prescription where p.PrescriptionId == drocid select p).ToList();
            tbl_Prescription data = dc.tbl_Prescription.Single(p => p.PrescriptionId == drocid);
            droc.DateOfVisit = data.DateOfVisit;
            droc.NextVisit = data.NextVisit;
            droc.HealthproblemDescription = data.HealthproblemDescription;
            droc.FileName = data.FileName;
            droc.PrescriptionId = data.PrescriptionId;
          List<tbl_PrescribedMedicine> pris= dc.tbl_PrescribedMedicine.Where(pd => pd.PrescriptionId == drocid).ToList();
           List<tbl_Document> drocument=  dc.tbl_Document.Where(d => d.PrescriptionID == drocid).ToList();

           droc.medicines = pris.Select(x => new MedicinesDTO1() {PrescribedMedicineId=x.PrescribedMedicineId, Dosage=x.Dosage,Duration=x.Duration,Nameofmedicine=x.NameOfMedicine,Quantity=x.Quantity }).ToList();
           droc.dro = drocument.Select(x => new Files() {DocumentID=x.DocumentID, drocName=x.DocumentName, fileUpload=x.DocumentPath }).ToList();

           return View(droc);
           
        }




        [HttpPost]
        public JsonResult EditDrocument(AddaDrocumentDTO1 objadddrocdto)
        {

            
            int id = WebSecurity.CurrentUserId;
            var EditDrocument = dc.tbl_Prescription.First(u => u.PrescriptionId == objadddrocdto.PrescriptionId);

            
            
                EditDrocument.PatientId = id;
               EditDrocument. DateOfVisit = objadddrocdto.DateOfVisit;
               EditDrocument. NextVisit = objadddrocdto.NextVisit;
               EditDrocument. HealthproblemDescription = objadddrocdto.HealthproblemDescription;
               EditDrocument. FileName = objadddrocdto.FileName;
               //EditDrocument. CreatedDate = DateTime.Now;
               EditDrocument. UpdatedDate = DateTime.Now;
               EditDrocument. Status = true;
               EditDrocument. DescriptionDate = DateTime.Now;
               EditDrocument. CreatedBy = id;
               EditDrocument.UpdatedBy = id;
         
            dc.SaveChanges();

            return Json("Drocument Added Successfully", JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult Editmedicine(MedicinesDTO1 objadddrocdto)
        {

          

            int id = WebSecurity.CurrentUserId;
            var pris = dc.tbl_PrescribedMedicine.First(u => u.PrescribedMedicineId == objadddrocdto.PrescribedMedicineId);
                    
                      pris.Duration = objadddrocdto.Duration;
                        pris. NameOfMedicine = objadddrocdto.Nameofmedicine;
                        // PrescriptionId = 7,
                       pris.  Dosage = objadddrocdto.Dosage;
                        pris. Quantity=objadddrocdto.Quantity;
                        pris. CreatedDate = System.DateTime.Now;
                        pris.UpdatedDate = System.DateTime.Now;
                    dc.SaveChanges();
              
            return Json("Drocument Added Successfully", JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult EditDrocuments(Files objaddfiledto)
        {
            int faileddocuments = 0;
            string myBucketName = ConfigurationManager.AppSettings["bucketname"]; //your s3 bucket name goes here
            string s3DirectoryName = "";
            string s3FileName = string.Format(@"{0}.txt", Guid.NewGuid());
            string AccessKey = ConfigurationManager.AppSettings["AWSAccessKey"].ToString();
            string SecretKey = ConfigurationManager.AppSettings["AWSSecretKey"].ToString();


            if (AmazonUploader.sendMyFileToS3(objaddfiledto.fileUpload, objaddfiledto.Format, myBucketName, "", s3FileName, AccessKey, SecretKey, RegionEndpoint.USWest2))
            {
                int id = WebSecurity.CurrentUserId;
                var pris = dc.tbl_Document.First(u => u.DocumentID == objaddfiledto.DocumentID);
                    
                var drocument = new tbl_Document
                {
                    UpdatedDate = System.DateTime.Now,
                    CreatedDate = System.DateTime.Now,
                    DocumentName = objaddfiledto.drocName,
                    CanShare = true,
                    Status = true,
                    DocumentPath = ConfigurationManager.AppSettings["awsUrl"].ToString() + ConfigurationManager.AppSettings["bucketname"].ToString() + "/" + s3FileName,
                    UpdatedBy = "",
                    CreatedBy = id
                };
              
                dc.SaveChanges();
                return Json("Drocument Added Successfully", JsonRequestBehavior.AllowGet);
            }
            else
            {
                faileddocuments++;
            }

            return Json("", JsonRequestBehavior.AllowGet);
          
        }


        public RedirectResult Download(string FileID)
        {

            return Redirect(FileID);
        }

    }
}
