﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace Drocuments.Controllers
{
    public class DashboardController : Controller
    {
        //
        // GET: /Dashboard/

        public ActionResult Index()
        {
            string username;
            var logonUser = Request.ServerVariables["LOGON_USER"];
           // username = logonUser.Split('\\')[1].ToString();
            
                if (WebSecurity.IsAuthenticated)
                {
                    username = logonUser;
                    var id = WebSecurity.CurrentUserId;

                    var cookie = new HttpCookie("FirstName");
                    Response.Cookies.Add(cookie);

                    //Get cookie
                    var val = Request.Cookies["FirstName"].Value;
                    return View();
                }
                else
                {


                    return RedirectToAction("Login");
                }


            
        }

    }
}
