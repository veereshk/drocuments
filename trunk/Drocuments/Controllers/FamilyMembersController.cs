﻿using Drocuments.Models.DTOs;
using Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

using System.Data.SqlClient;
using System.Data;
namespace Drocuments.Controllers
{

    public class FamilyMembersController : Controller
    {
        //
        // GET: /FamilyMembers/
        DrocumentsEntities dc = new DrocumentsEntities();

        public ActionResult Index()
        {
            if (TempData["Message"] != null)
            {
                ViewBag.Message = TempData["Message"].ToString();
            }
            int id = WebSecurity.CurrentUserId;
            // dc.Configuration.ProxyCreationEnabled = false;
            var data = dc.tbl_Family_Member.Where(p => p.UserID == id ).ToList();
            return View(data);
        }
        public ActionResult Add()
        {

            FamilyMembers model = new FamilyMembers();
            int id = WebSecurity.CurrentUserId;
            model.UserID = id;
            return View(model);
           
            
        }
        [HttpPost]
        public ActionResult Add(FamilyMembers objaddfamilymembers)
        {
            //int id = WebSecurity.CurrentUserId;
            tbl_Family_Member family = new tbl_Family_Member
            {
                UserID = objaddfamilymembers.UserID,
                UpdatedBy = objaddfamilymembers.UserID,
                CreatedOn = objaddfamilymembers.CreatedOn,
                IsActive = true,
                UpdatedOn = DateTime.Now,
                FamilyMemberEmail = objaddfamilymembers.FamilyMemberEmail,
                CreatedBy = objaddfamilymembers.UserID,
                Status = objaddfamilymembers.Status
            };
            dc.tbl_Family_Member.Add(family);
            dc.SaveChanges();
            return View(objaddfamilymembers);

            
                   }
        
  
//            @Html.DayPilotScheduler("dps", new DayPilotSchedulerConfig
//{
//    BackendUrl = Url.Content("~/Scheduler/Backend"),
//    CssOnly = true,
//    CssClassPrefix = "scheduler_white"
//})
        }

    }

