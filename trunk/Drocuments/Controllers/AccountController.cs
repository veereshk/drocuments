﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using Drocuments.Filters;
using Drocuments.Models;
using Repository;
using System.Configuration;


namespace Drocuments.Controllers
{
 
    public class AccountController : Controller
    {
        DrocumentsEntities dc = new DrocumentsEntities();
        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (TempData["Message"] != null)
            {
                ViewBag.Message = TempData["Message"].ToString();
            }
           // ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
       
        public ActionResult Login(string Email, string password)
        {

            bool success = WebSecurity.Login(Email, password, false);
            if (success)
            {
                string returnUrl = Request.QueryString["ReturnUrl"];
                if (returnUrl == null)
                {
                  //  RedirectToAction("Index", "Dashboard");
                    Response.Redirect("~/Dashboard");
                }
                else
                {

                    return RedirectToAction(returnUrl);
                }

            }

            TempData["Message"] = "Please provide valid credentials";
            return RedirectToAction("Login");
        }

        //
        // POST: /Account/LogOff

        [HttpGet]
        public ActionResult LogOut()
        {
            WebSecurity.Logout();
            return RedirectToAction("Login", "Account");
        }

        //
        // GET: /Account/Register

        [AllowAnonymous]
        public ActionResult Register()
        {
            if (TempData["Message"] != null)
            {
                ViewBag.Message = TempData["Message"].ToString();
            }
            ViewBag.CityId = new SelectList(dc.tbl_City, "CityID", "CityName");
            return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(tbl_UserInfo u, tbl_LogIn log)
        {

            tbl_UserInfo email = dc.tbl_UserInfo.FirstOrDefault(m => m.Email.ToLower() == log.Email.ToLower());
            // Check if email already exists
            if (email == null)
            {
                List<string> ObjRegistrationData = new List<string>();
                if (RegisterUser(u.FirstName, u.LastName, u.Gender, u.Email, u.Password, u.CityId, u.ContactNumber, u.Role, u.Address, 1))
                    return RedirectToAction("Thankyou", "Account");
                else
                    return RedirectToAction("Register");
            }
            else
            {
                TempData["Message"] = "Email address already exists. Please enter a different email address.";
                //ModelState.AddModelError("Email", "Email address already exists. Please enter a different email address.");
            }
            return RedirectToAction("Register");
        }

         [AllowAnonymous]
        public ActionResult Thankyou()
        {
            return View();
        }


         [AllowAnonymous]
        [HttpGet]
        public ActionResult ActivateAccount(string AccessToken)
        {
            if (WebSecurity.ConfirmAccount(AccessToken))
            {
                TempData["Message"] = "Congratulations . Your Account has been activated . Enjoy our services !!!";
                return RedirectToAction("Thankyou");
            }
            else
            {
                return View();
            }

        }

         [AllowAnonymous]
        private bool RegisterUser(string fname, string lname, bool gender, string email, string password, int? cityid, long? phno, int? role, string address, int methodofregistration)
        {
            try
            {
                MailMessage objmailmessage = new MailMessage();
                gender = true;
                string Subject = "";
                string ConfirmationToken = WebSecurity.CreateUserAndAccount(email, password, new { FirstName = fname, LastName = lname, ContactNumber = phno, Gender = gender, Address = address, CityId = cityid, CreatedBy = 1, CreatedDate = DateTime.Now, UpdatedDate = DateTime.Now, UpdatedBy = 1 }, true);

                var ActivationUrl = Server.HtmlEncode(ConfigurationManager.AppSettings["websiteurl"].ToString() + "/Account/ActivateAccount?AccessToken=" + ConfirmationToken);
                string Body = "";

                if (methodofregistration == 1)
                {
                    Subject = "Drocuments - Confirm Subscription";
                    Body = "Hi " + fname + " " + lname + "!\n" +
                         "Thanks for showing interest and registring in <a href='http://www.Drocument.com'><a> " +
                         " Please <a href='" + ActivationUrl + "'>click here to activate</a>  your account and enjoy our services. \nThanks!";
                }
                else if (methodofregistration == 2)
                {
                    Subject = "Drocuments - Account Activation Info";
                    Body = "Hi " + fname + " " + lname + "!\n" +
                           "Thanks for showing interest and registring in <a href='http://www.Drocument.com'><a> " +
                           "Your Accout is ready and you can access your account and enjoy our services<br> Please click <a  href=" + ConfigurationManager.AppSettings["websiteurl"].ToString() + ">here</a>  to start using Drocuments.";
                }
                objmailmessage.SendMail(email, Body, Subject);
                TempData["Message"] = "Please check your email to activate your account.";
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

         [AllowAnonymous]
        [HttpGet]
public ActionResult Details()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Details(FormCollection collection)
        {
            var id = WebSecurity.CurrentUserId;
            if (ModelState.IsValid)
            {
                var user = dc.tbl_UserInfo.First(u => u.UserId == id);
                user.FirstName = collection["DateofBirth"];
                user.LastName = collection["Weight"];
                user.Address = collection["BloodGroup"];
                user.Address = collection["City"];
                user.Address = collection["Country"];
                user.ContactNumber = Convert.ToInt64(collection["ContactNumber"].ToString());
                dc.SaveChanges();
            }

            return RedirectToAction("Details", "Account");
        }

         [AllowAnonymous]
        [HttpGet]
        public ActionResult Facebook(string provider)
        {
            OAuthWebSecurity.RequestAuthentication(provider, Url.Action("Facebooklogincallback"));
            return RedirectToAction("Index", "Dashboard", new { area = "Common" });
        }
         [AllowAnonymous]

        public ActionResult Facebooklogincallback(string returnUrl)
        {
            DotNetOpenAuth.AspNet.AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication();
            if (result.IsSuccessful == false)
            {

                TempData["msg"] = "LoginFailed";
                return RedirectToAction("Login");

            }
            else
            {
                // validate emailid against database and if emailid exists then get userid and email address from db and redirect him to home 
                //else add data to database 

                string emailaddress = result.ExtraData["username"].ToString();
                string[] fullname = result.ExtraData["name"].ToString().Split(' ').ToArray();
                string gender = result.ExtraData["gender"].ToString();
                bool objgender = false;
                fullname[0] = fullname[0] == null ? "" : fullname[0];
                fullname[1] = fullname[1] == null ? "" : fullname[1];

                // if emailid is there 

                if (gender.ToLower() == "male")
                {
                    objgender = true;
                }
                if (dc.tbl_UserInfo.Where(x => x.Email == emailaddress).Any())
                {
                    FormsAuthentication.RedirectFromLoginPage(emailaddress, false);

                    return RedirectToAction("Index", "Dashboard", new { area = "Common" });
                }
                else
                {
                    // Genereate random password , for now used weak method , but we have to add reliable alg, and send it to  user 
                    string password = DateTime.Now.Millisecond.ToString() + DateTime.Now.Millisecond.ToString() + DateTime.Now.Millisecond.ToString() + DateTime.Now.Millisecond.ToString();
                    if (RegisterUser(fullname[0], fullname[1], objgender, emailaddress, password, 1, 000000, 1, "Hyderabad", 2))
                        return RedirectToAction("Thankyou", "Account");
                    else
                        return RedirectToAction("Register");

                }

            }
        }


        public ActionResult Gmail(string provider)
        {
            OAuthWebSecurity.RequestAuthentication(provider, Url.Action("Gmaillogincallback"));
            return RedirectToAction("Index", "Dashboard", new { area = "Common" });
        }


        public ActionResult Gmaillogincallback(string returnUrl)
        {

            DotNetOpenAuth.AspNet.AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication();



            if (result.IsSuccessful == false)
            {

                TempData["msg"] = "LoginFailed";
                return RedirectToAction("Login");

            }
            else
            {
                // validate emailid against database and if emailid exists then get userid and email address from db and redirect him to home 
                //else add data to database 

                string gender = "";

                string email = result.ExtraData["email"].ToString();

                string[] fullname = new string[2];
                string country;
                string address = "HYD";

                if (result.ExtraData.ContainsKey("firstName"))
                    fullname[0] = result.ExtraData["firstName"].ToString();

                if (result.ExtraData.ContainsKey("lastName"))
                    fullname[1] = result.ExtraData["lastName"].ToString();

                if (result.ExtraData.ContainsKey("name"))
                    fullname = result.ExtraData["name"].ToString().Split(' ').ToArray();

                if (result.ExtraData.ContainsKey("country"))
                    address = result.ExtraData["country"].ToString();

                if (result.ExtraData.ContainsKey("gender"))
                    gender = result.ExtraData["gender"].ToString();

                bool objgender = false;
                fullname[0] = fullname[0] == null ? "" : fullname[0];
                fullname[1] = fullname[1] == null ? "" : fullname[1];

                // if emailid is there 

                if (gender.ToLower() == "male")
                {
                    objgender = true;
                }
                if (dc.tbl_UserInfo.Where(x => x.Email == email).Any())
                {
                    FormsAuthentication.RedirectFromLoginPage(email, false);
                    return RedirectToAction("Index", "Dashboard", new { area = "Common" });
                }
                else
                {
                    // Genereate random password , for now used weak method , but we have to add reliable alg, and send it to  user 
                    string password = DateTime.Now.Millisecond.ToString() + DateTime.Now.Millisecond.ToString() + DateTime.Now.Millisecond.ToString() + DateTime.Now.Millisecond.ToString();
                    if (RegisterUser(fullname[0], fullname[1], objgender, email, password, 1, 000000, 1, address, 2))
                        return RedirectToAction("Thankyou", "Account");
                    else
                        return RedirectToAction("Register");

                }

            }
        }





        //
        // POST: /Account/Disassociate

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Disassociate(string provider, string providerUserId)
        {
            string ownerAccount = OAuthWebSecurity.GetUserName(provider, providerUserId);
            ManageMessageId? message = null;

            // Only disassociate the account if the currently logged in user is the owner
            if (ownerAccount == User.Identity.Name)
            {
                // Use a transaction to prevent the user from deleting their last login credential
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
                    if (hasLocalAccount || OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name).Count > 1)
                    {
                        OAuthWebSecurity.DeleteAccount(provider, providerUserId);
                        scope.Complete();
                        message = ManageMessageId.RemoveLoginSuccess;
                    }
                }
            }

            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage

        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : "";
            ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                    }
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError("", String.Format("Unable to create local account. An account with the name \"{0}\" may already exist.", User.Identity.Name));
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ExternalLoginFailure

        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult ExternalLoginsList(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return PartialView("_ExternalLoginsListPartial", OAuthWebSecurity.RegisteredClientData);
        }

        [ChildActionOnly]
        public ActionResult RemoveExternalLogins()
        {
            ICollection<OAuthAccount> accounts = OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name);
            List<ExternalLogin> externalLogins = new List<ExternalLogin>();
            foreach (OAuthAccount account in accounts)
            {
                AuthenticationClientData clientData = OAuthWebSecurity.GetOAuthClientData(account.Provider);

                externalLogins.Add(new ExternalLogin
                {
                    Provider = account.Provider,
                    ProviderDisplayName = clientData.DisplayName,
                    ProviderUserId = account.ProviderUserId,
                });
            }

            ViewBag.ShowRemoveButton = externalLogins.Count > 1 || OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            return PartialView("_RemoveExternalLoginsPartial", externalLogins);
        }
        [HttpGet]
        public ActionResult ResetPassword(string AccessToken)
        {
            ViewBag.PasswordResetToken = AccessToken;
            return View();

        }

        [HttpPost]
        public ActionResult ResetPassword(string AccessToken, string Password)
        {
            if (WebSecurity.ResetPassword(AccessToken, Password))
            {
                TempData["Message"] = "Your Password is successfully changed. Please login again with your new password !!!";
                return RedirectToAction("Thankyou");
            }
            else
            {
                return View();
            }
        }

        public ActionResult forgotpassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult forgotpassword(string Email)
        {
            if (WebSecurity.GetUserId(Email) > 0)
            {

                string passwordtoken = WebSecurity.GeneratePasswordResetToken(Email, 1440);
                if (passwordtoken != null)
                {
                    //send mail 
                    MailMessage objmailmessage = new MailMessage();
                    string Subject = "Drocuments - Reset Password !!!";
                    var ActivationUrl = Server.HtmlEncode(ConfigurationManager.AppSettings["websiteurl"].ToString() + "/Account/ResetPassword?AccessToken=" + passwordtoken);
                    string Body = "Hi " + Email + "\n" + " Please <a href='" + ActivationUrl + "'>click here to rest </a>  your account. \nThanks!";
                    objmailmessage.SendMail(Email, Body, Subject);
                    ViewBag.Message = "Password has been sent to your registered email address";
                }
                else
                {
                    ViewBag.Message = "Failed to reset password";
                }
            }
            else
            {
                ViewBag.Message = "There is no user associated with this Email Address";
            }
            return View();
        }

        public ActionResult Edit()
        {
            int id = WebSecurity.CurrentUserId;
            dc.Configuration.ProxyCreationEnabled = false;
            tbl_UserInfo data = dc.tbl_UserInfo.FirstOrDefault(p => p.UserId == id);
            return View(data);

        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection)
        {
            var id = WebSecurity.CurrentUserId;
            if (ModelState.IsValid)
            {
                var user = dc.tbl_UserInfo.First(u => u.UserId == id);
                user.FirstName = collection["FirstName"];
                user.LastName = collection["LastName"];
                user.Address = collection["Address"];
                user.ContactNumber = Convert.ToInt64(collection["ContactNumber"].ToString());
                dc.SaveChanges();
            }

            return RedirectToAction("Edit", "Account");
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Account");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
