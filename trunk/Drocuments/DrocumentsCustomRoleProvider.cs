﻿using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Drocuments
{
    public class DrocumentsCustomRoleProvider : RoleProvider
    {
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            using (DrocumentsEntities db = new DrocumentsEntities())
            {
                tbl_LogIn user = db.tbl_LogIn.FirstOrDefault(u => u.Email.Equals(username, StringComparison.CurrentCultureIgnoreCase));

                var roles = from ur in db.tbl_LogIn
                            from r in db.tbl_Role
                            where ur.RoleId == r.RoleId
                            select r.RoleName;
                if (roles != null)
                    return roles.ToArray();
                else
                    return new string[] { };
            }
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            using (DrocumentsEntities db = new DrocumentsEntities())
            {
                tbl_LogIn user = db.tbl_LogIn.FirstOrDefault(u => u.Email.Equals(username, StringComparison.CurrentCultureIgnoreCase));

                var roles = from ur in db.tbl_LogIn
                            from r in db.tbl_Role
                            where ur.RoleId == r.RoleId
                            select r.RoleName;
                if (user != null)
                    return roles.Any(r => r.Equals(roleName, StringComparison.CurrentCultureIgnoreCase));
                else
                    return false;
            }
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}