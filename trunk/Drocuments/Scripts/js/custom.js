﻿menuSelect();
function menuSelect() {
    var url = window.location.pathname;

    $(".dashboard-menu a").each(function () {
        //Chrome Support for .contains() START
        if (!('contains' in String.prototype)) {
            String.prototype.contains = function (str, startIndex) {
                return -1 !== String.prototype.indexOf.call(this, str, startIndex);
            };
        }
        //Chrome Support for .contains() END
        if (url == $(this).attr('href')) {
            $(this).addClass("active");
        }
    });

}
function GetprofileInfo() {

    var json = CheckLogin();

    jQuery.support.cors = true;
    $.ajax({
        url: '../api/Master/GetProfileDetails?id=' + json['UserId'],
        type: 'GET',
        dataType: 'json',
        success: function (data) {

            $("#FirstName").val(data.Result.FirstName),
$("#LastName").val(data.Result.LastName),
$("#Address").val(data.Result.Address),
$("#ContactNumber").val(data.Result.MobileNo)

        }

    });

}
var tblmedicine = new Array();
var tbldrocument = new Array();
var drocument = '';
//global variables 



function ClearGlobalCacheData() {
    tblmedicine.length = 0;
    tbldrocument.length = 0;
    drocument = '';
    documentextenstion = '';
    base64string = '';


}
$(document).ready(function () {

    //$('#fileUpload').change(function (evt) {

    //var filename = $(this).val();
    //var documentextenstion = filename.replace(/^.*\./, '');
    //var docname = $(this).parents('.drocument').find('.drocumentname').val();
    //var fileReader = new FileReader(),
    //    files = this.files,
    //    file;
    //if (!files.length) {
    //    return;
    //}
    //file = files[0];
    //fileReader.readAsDataURL(file);
    //fileReader.onload = function (_file) {
    //    var url = _file.target.result;
    //    tbldrocument.push({ drocName: docname, fileUpload: url.substring(url.indexOf("base64") + 7), Format: documentextenstion });
    //};

    //  });

    $("#addmedicine").click(function () {

        var id = 0;
        $('.medicine').each(function () {

            if (tblmedicine == undefined || tblmedicine.length == 0) {
                id = 0;
            }
            else {
                //get the max id from tblmedicine array and increament by 1 and assign it to id
                $.each(tblmedicine, function (index, item) {
                    if (item.PrescribedMedicineId >= id) {
                        id = item.PrescribedMedicineId;
                    }
                });
            }
            id = id + 1;//generate next id

            var tr = '<tr>';
            tr += '<td>' + $(this).find("#Nameofmedicine").val() + '<input id=medicineId class=medidicineid type=hidden isnew=1 value=' + id + '></input>' + '</td>';
            tr += '<td>' + $(this).find("#Duration").val() + '</td>';
            tr += '<td>' + $(this).find("#Quantity").val() + '</td>';
            tr += '<td>' + $(this).find('#Dosage').val().join(",") + '</td>';
            tr += '<td><a class="btn btn-link deleteRowmainMedicine"><b>X</b></a></td>'
            tr += '</tr>';
            $('#maintblmedicines tr:last').after(tr);

            var medic = getmedicine($(this),id);
            tblmedicine.push(medic);
        });
        //$('myModal').removeClass('.medicine:not(:first)');
        $('button[class=close]').click();

    });
    $("#adddrocument").click(function () {
        var id = 0;

        $('.drocument').each(function () {
            if (tbldrocument == undefined || tbldrocument.length == 0) {
                    id = 0;
                }
                else {
                    //get the max id from tblmedicine array and increament by 1 and assign it to id
                $.each(tbldrocument, function (index, item) {
                    if (item.DocumentID >= id) {
                        id = item.DocumentID;
                        }
                    });
                }
                id = id + 1;//generate next id
                var tr = '<tr>';
                tr += '<td>' + $(this).find("#drocName").val() + '<input id=drocumentid  class=drodrcumentid  type=hidden isnew=1 value=' + id + '></input>' + '</td>';
                tr += '<td>' + $(this).find("#fileUpload").val() + '</td>';
            tr += '<td><a class="btn btn-link deleteRowmaindrocument"><b>X</b></a></td>'
            tr += '</tr>';
            $('#maintbldrocument tr:last').after(tr);

            getdrocument($(this),id);
            //   tbldrocument.push(drocs);

        });
        $('button[class=close]').click();
    });

    $("#adddrocumentedit").click(function () {
        $('.drocument').each(function () {
            var tr = '<tr>';
            tr += '<td>' + $(this).find("#drocName").val() + '</td>';
            tr += '<td>' + $(this).find("#fileUpload").val() + '</td>';
            tr += '<td><a class="btn btn-link deleteRowmaindrocument"><b>X</b></a></td>'
            tr += '</tr>';
            $('#maintbldrocument tr:last').after(tr);

            getdrocument($(this),id);
            //   tbldrocument.push(drocs);

        });
        $('button[class=close]').click();
    });
    

    $("#addmedicineedit").click(function () {

        $('.medicine').each(function () {
            var tr = '<tr>';
            tr += '<td>' + $(this).find("#Nameofmedicine").val() + '</td>';
            tr += '<td>' + $(this).find("#Duration").val() + '</td>';
            tr += '<td>' + $(this).find("#Quantity").val() + '</td>';
            tr += '<td>' + $(this).find('#Dosage').val().join(",") + '</td>';
            tr += '<td><a class="btn btn-link deleteRowmainMedicine"><b>X</b></a></td>'
            tr += '</tr>';
            $('#maintblmedicines tr:last').after(tr);

            var medic = getmedicine($(this), id);
            tblmedicine.push(medic);
        });
        //$('myModal').removeClass('.medicine:not(:first)');
        $('button[class=close]').click();

    });


    $(document).ready(function () {
        $("#savedrocument").click(function () {
            debugger;
            drocumentdata = {
                PrescriptionId:$("#PrescriptionId").val(),
                FileName: $("#DocumentName").val(),
                DateOfVisit: $("#DateOfVisit").val(),
                NextVisit: $("#NextVisit").val(),
                HealthproblemDescription: $("#HealthproblemDescription").val(),
                medicines: tblmedicine, dro: tbldrocument
            };


            var DTO = JSON.stringify(drocumentdata);
            $.ajax({
                url: '/Drocuments/Add', //calling Web API controller product
                cache: false,
                type: 'POST', async: false,
                contentType: 'application/json; charset=utf-8',
                data: DTO,
                dataType: 'json',
                //contentType: false,
                //processData: false,
                success: function (data) {
                    alert('added');
                    ClearGlobalCacheData();
                }
            }).fail(
            function (xhr, textStatus, err) {
                alert(err);
            });
        });
    });






    function getmedicine(ele,id) {

        var Name = ele.find("#Nameofmedicine").val();
        var Durat = ele.find("#Duration").val();
        var qty = ele.find("#Quantity").val();
        var names = $('#Dosage').val().join(",");

        return { PrescribedMedicineId:id, Nameofmedicine: Name, Duration: Durat, Dosage: names, Quantity: qty };
    }
    function getdrocument(ele,id) {

        var filename = ele.find("#fileUpload").val();
        var documentextenstion = filename.replace(/^.*\./, '');
        var docname = ele.find("#drocName").val();
        var fileReader = new FileReader();
        var files = ele.find("#fileUpload")[0].files;
        var file;
        if (!files.length) {
            return;
        }
        file = files[0];
        fileReader.readAsDataURL(file);
        fileReader.onload = function (_file) {
            var url = _file.target.result;
            tbldrocument.push({DocumentID:id, drocName: docname, fileUpload: url.substring(url.indexOf("base64") + 7), Format: documentextenstion });
        };
    }



    function deleterowmedicinepop() {

        $('#myModal').find('.medicine:not(:first)').remove();
        $('.medicine').find('input').val("");
        $('.medicine').find('#Dosage').first().multipleSelect();
        $('.medicine').find('#Dosage').first().find('[name=options]').attr("selected", false);
    }


    function deleterowdrocumentpop() {

        $('#uploadDocuments').find('.drocument:not(:first)').remove();
        $('.drocument').find('input').val("");
    }



    //$(document).on('click', '.deleteRowmaindrocument', function () {
    //    var tr = $(this).parents('tr:first');
    //    var id = tr.find("#drocumentid").val();
    //    //tr.find('.edit-mode, .display-mode').toggle();
    //    if (confirm("Do you want to delete Id: " + id)) {
    //        var data = { 'DocumentID': id }

    //    }
    //    $(this).closest('tr').remove();

    //    tbldrocument = $.grep(tbldrocument, function (item, index) {
    //        return item.DocumentID != id;
    //    });
    //});



    //$(document).on('click', '.deleteRowmainMedicine', function () {

      

    //        var tr = $(this).parents('tr:first');
    //        var id = tr.find("#medicineId").val();
    //        //tr.find('.edit-mode, .display-mode').toggle();
    //        if (confirm("Do you want to delete Id: " + id)) {
    //            $(this).closest('tr').remove();

    //            tblmedicine = $.grep(tblmedicine, function (item, index) {
    //                return item.PrescribedMedicineId != id;
    //            });

    //        }

    //        else {
    //            return false;
    //        }
           
        
    //    // delete tblmedicine.drocid


    //});

});