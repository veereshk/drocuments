﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Drocuments.Models.DTOs
{
   
    public partial class Files
    {
        public int DocumentID { get; set; }
        public string drocName { get; set; }
        public string fileUpload { get; set; }
        public string Format { get; set; }
    }
    public class MedicinesDTO1
    {
        public int PrescribedMedicineId { get; set; }
        public string Nameofmedicine { get; set; }
        public string Duration { get; set; }
        public string Dosage { get; set; }
        public string Quantity { get; set; }
    }

    public class AddaDrocumentDTO1
    {
        public int PrescriptionId { get; set; }
        public string FileName { get; set; }
        public DateTime DateOfVisit { get; set; }
        public DateTime NextVisit { get; set; }
        public string HealthproblemDescription { get; set; }
       

    }


    public class AddaDrocumentDTO
    {
        public int PrescriptionId { get; set; }
        public string FileName { get; set; }
        public DateTime DateOfVisit { get; set; }
        public DateTime NextVisit { get; set; }
        public string HealthproblemDescription { get; set; }
        public List<Files> dro { get; set; }
        public List<MedicinesDTO1> medicines { get; set; }


    }
}