﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Drocuments.Models.DTOs
{
    public class Doctorsmodel
    {
        public int id { set; get; }
        public string name { set; get; }
        public int specialization_id { set; get; }
        public string clinic_name { set; get; }
        public string address { set; get; }
        public string qualification { set; get; }
        public int rating { set; get; }
    }
}