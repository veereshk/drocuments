﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Drocuments.Models.DTOs
{
    public class FamilyMembers
    {
        public int Id { get; set; }
        public Nullable<int> UserID { get; set; }
        public Nullable<int> FamilyMemberId { get; set; }
        public string FamilyMemberEmail { get; set; }
        public Nullable<int> RelationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<bool> Status { get; set; }

    }
     public class relations
     {
         public int RelationId { get; set; }
         public string RelationName { get; set; }
         public bool StatusId { get; set; }
         public System.DateTime CreatedDate { get; set; }
         public System.DateTime UpdatedDate { get; set; }
     }
}


  