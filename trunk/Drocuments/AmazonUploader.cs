﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Amazon;
using System.IO;
using Amazon.S3.Model;
using Amazon.S3;
using Amazon.S3.Transfer;

namespace Drocuments
{
    public static class AmazonUploader
    {
        public static bool sendMyFileToS3(string base64string, string Format, string bucketName, string subDirectoryInBucket, string fileNameInS3, string AWSAccessKey, string AWSSecretKey, RegionEndpoint regionendpoint)
        {
            try
            {
                IAmazonS3 client;
                byte[] bytes = Convert.FromBase64String(base64string);
                using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(AWSAccessKey, AWSSecretKey, regionendpoint))
                {
                    var request = new PutObjectRequest
                    {
                        BucketName = bucketName,
                        CannedACL = S3CannedACL.PublicRead,
                        Key = fileNameInS3
                    };
                    using (var ms = new MemoryStream(bytes))
                    {
                        request.ContentType = "image/" + Format;
                        request.InputStream = ms;
                        client.PutObject(request);
                    }

                    return true;
                }
                //if (PostedFileBase.ContentLength > 0)
                //{
                //    Amazon.S3.IAmazonS3 client = Amazon.AWSClientFactory.CreateAmazonS3Client(AWSAccessKey, AWSSecretKey, regionendpoint);
                //    TransferUtility utility = new TransferUtility(client);
                //        TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();
                //        if (subDirectoryInBucket == "" || subDirectoryInBucket == null)
                //        {
                //            request.BucketName = bucketName; //no subdirectory just bucket name
                //        }
                //        else
                //        {   // subdirectory and bucket name
                //            request.BucketName = bucketName + @"/" + subDirectoryInBucket;
                //        }
                //        request.Key = fileNameInS3;

                //        request.InputStream = PostedFileBase.InputStream;
                //        utility.Upload(request);
                //        client.Dispose();
                //        return true;
                //}
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }
    }
}